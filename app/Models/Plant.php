<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plant extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'plants';
    public $timestamps = false;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'scientific_name',
        'species',
        'description',
        'minimum_age',
        'maximum_age',
        'image_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
