<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhenologiacaObservation extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'phenologiaca_observations';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'description',
        'area_id',
        'plant_id',
        'event_genre_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function plant()
    {
        return $this->belongsTo(Plant::class, 'plant_id');
    }

    public function event_genre()
    {
        return $this->belongsTo(EventGenre::class, 'event_genre_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
