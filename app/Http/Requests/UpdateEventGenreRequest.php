<?php

namespace App\Http\Requests;

use App\Models\EventGenre;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateEventGenreRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('event_genre_edit');
    }

    public function rules()
    {
        return [
            'event_name' => [
                'string',
                'min:2',
                'max:50',
                'nullable',
            ],
        ];
    }
}
