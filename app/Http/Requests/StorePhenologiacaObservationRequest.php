<?php

namespace App\Http\Requests;

use App\Models\PhenologiacaObservation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePhenologiacaObservationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('phenologiaca_observation_create');
    }

    public function rules()
    {
        return [
            'description' => [
                'string',
                'min:2',
                'max:500',
                'nullable',
            ],
        ];
    }
}
