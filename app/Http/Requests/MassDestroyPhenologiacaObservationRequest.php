<?php

namespace App\Http\Requests;

use App\Models\PhenologiacaObservation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyPhenologiacaObservationRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('phenologiaca_observation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:phenologiaca_observations,id',
        ];
    }
}
