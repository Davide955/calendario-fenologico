<?php

namespace App\Http\Requests;

use App\Models\Contact;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreContactRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('contact_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'min:2',
                'max:30',
                'nullable',
            ],
            'request' => [
                'string',
                'min:2',
                'max:500',
                'nullable',
            ],
            'lastname' => [
                'string',
                'min:2',
                'max:30',
                'nullable',
            ],
            'email' => [
                'string',
                'min:2',
                'max:50',
                'nullable',
            ],
        ];
    }
}
