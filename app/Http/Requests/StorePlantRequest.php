<?php

namespace App\Http\Requests;

use App\Models\Plant;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePlantRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('plant_create');
    }

    public function rules()
    {
        return [
            'scientific_name' => [
                'string',
                'min:2',
                'max:50',
                'nullable',
            ],
            'species' => [
                'string',
                'min:2',
                'max:50',
                'nullable',
            ],
            'description' => [
                'string',
                'min:2',
                'max:500',
                'nullable',
            ],
            'minimum_age' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'maximum_age' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
