<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plant;
use Illuminate\Http\Request;
//Prima si chiamava HomeController
class HomeController
{
    public function index()
    {
        return view('home');
    }
    public function saveDataPlants(Request $req){
        print_r($req->input());
        $plant = new Plant();
        $plant->scientific_name = $req->scientific_name;
        $plant->species= $req->species;
        $plant->description = $req->description;
        $plant->minimum_age = $req->minimum_age;
        $plant->maximum_age = $req->maximum_age;
        echo $plant->save();



    }
}
