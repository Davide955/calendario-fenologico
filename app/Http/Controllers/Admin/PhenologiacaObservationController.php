<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPhenologiacaObservationRequest;
use App\Http\Requests\StorePhenologiacaObservationRequest;
use App\Http\Requests\UpdatePhenologiacaObservationRequest;
use App\Models\Area;
use App\Models\EventGenre;
use App\Models\PhenologiacaObservation;
use App\Models\Plant;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PhenologiacaObservationController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('phenologiaca_observation_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologiacaObservations = PhenologiacaObservation::with(['area', 'plant', 'event_genre'])->get();

        return view('admin.phenologiacaObservations.index', compact('phenologiacaObservations'));
    }

    public function create()
    {
        abort_if(Gate::denies('phenologiaca_observation_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $areas = Area::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $plants = Plant::pluck('scientific_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $event_genres = EventGenre::pluck('event_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.phenologiacaObservations.create', compact('areas', 'event_genres', 'plants'));
    }

    public function store(StorePhenologiacaObservationRequest $request)
    {
        $phenologiacaObservation = PhenologiacaObservation::create($request->all());

        return redirect()->route('admin.phenologiaca-observations.index');
    }

    public function edit(PhenologiacaObservation $phenologiacaObservation)
    {
        abort_if(Gate::denies('phenologiaca_observation_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $areas = Area::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $plants = Plant::pluck('scientific_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $event_genres = EventGenre::pluck('event_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $phenologiacaObservation->load('area', 'plant', 'event_genre');

        return view('admin.phenologiacaObservations.edit', compact('areas', 'event_genres', 'phenologiacaObservation', 'plants'));
    }

    public function update(UpdatePhenologiacaObservationRequest $request, PhenologiacaObservation $phenologiacaObservation)
    {
        $phenologiacaObservation->update($request->all());

        return redirect()->route('admin.phenologiaca-observations.index');
    }

    public function show(PhenologiacaObservation $phenologiacaObservation)
    {
        abort_if(Gate::denies('phenologiaca_observation_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologiacaObservation->load('area', 'plant', 'event_genre');

        return view('admin.phenologiacaObservations.show', compact('phenologiacaObservation'));
    }

    public function destroy(PhenologiacaObservation $phenologiacaObservation)
    {
        abort_if(Gate::denies('phenologiaca_observation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $phenologiacaObservation->delete();

        return back();
    }

    public function massDestroy(MassDestroyPhenologiacaObservationRequest $request)
    {
        PhenologiacaObservation::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
