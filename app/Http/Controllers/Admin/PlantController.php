<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPlantRequest;
use App\Http\Requests\StorePlantRequest;
use App\Models\Image;
use App\Models\Plant;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PlantController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('plant_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plants = Plant::with(['image'])->get();

        return view('admin.plants.index', compact('plants'));
    }

    public function create()
    {
        abort_if(Gate::denies('plant_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $images = Image::pluck('description', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.plants.create', compact('images'));
    }

    public function store(StorePlantRequest $request)
    {
        $plant = Plant::create($request->all());

        return redirect()->route('admin.plants.index');
    }

    public function show(Plant $plant)
    {
        abort_if(Gate::denies('plant_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plant->load('image');

        return view('admin.plants.show', compact('plant'));
    }

    public function destroy(Plant $plant)
    {
        abort_if(Gate::denies('plant_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $plant->delete();

        return back();
    }

    public function massDestroy(MassDestroyPlantRequest $request)
    {
        Plant::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
