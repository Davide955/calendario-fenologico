<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEventGenreRequest;
use App\Http\Requests\StoreEventGenreRequest;
use App\Http\Requests\UpdateEventGenreRequest;
use App\Models\EventGenre;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EventGenreController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('event_genre_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $eventGenres = EventGenre::all();

        return view('admin.eventGenres.index', compact('eventGenres'));
    }

    public function create()
    {
        abort_if(Gate::denies('event_genre_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.eventGenres.create');
    }

    public function store(StoreEventGenreRequest $request)
    {
        $eventGenre = EventGenre::create($request->all());

        return redirect()->route('admin.event-genres.index');
    }

    public function edit(EventGenre $eventGenre)
    {
        abort_if(Gate::denies('event_genre_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.eventGenres.edit', compact('eventGenre'));
    }

    public function update(UpdateEventGenreRequest $request, EventGenre $eventGenre)
    {
        $eventGenre->update($request->all());

        return redirect()->route('admin.event-genres.index');
    }

    public function show(EventGenre $eventGenre)
    {
        abort_if(Gate::denies('event_genre_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.eventGenres.show', compact('eventGenre'));
    }

    public function destroy(EventGenre $eventGenre)
    {
        abort_if(Gate::denies('event_genre_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $eventGenre->delete();

        return back();
    }

    public function massDestroy(MassDestroyEventGenreRequest $request)
    {
        EventGenre::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
