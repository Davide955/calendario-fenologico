<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class RichiestaDatiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function registrati(Request $req)
    {
//        print_r($req->input());
        //Creazione nuovo oggetto User
        $user = new User();
        //Campi richiesti in input presi dalla classe User
        //- nome
        //- email
        //- password
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        //con l'immissione della password viene criptata e resa sicura
        $user->password = Crypt::encrypt(($req->input('password')));
        //i dati vengono salvati nel db
        $user->save();
        $req->session()->put('user', $req->input('name'));
        return redirect('/home');

    }
//    public function login(Request $req)
//    {
//        $user = User::where('email', $req->input('email'))->get();
//
////        return Crypt::decrypt($user[0]->password);
//        if(Crypt::decrypt($user[0]->password)==$req->input('password'))
//        {
//            $req->session()->put('user', $user[0]->name);
//            redirect('/home');
//        }
//
//
//
//    }
}
