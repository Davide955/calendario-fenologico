<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Area
    Route::delete('areas/destroy', 'AreaController@massDestroy')->name('areas.massDestroy');
    Route::resource('areas', 'AreaController');

    // Place
    Route::delete('places/destroy', 'PlaceController@massDestroy')->name('places.massDestroy');
    Route::resource('places', 'PlaceController');

    // Plant
    Route::delete('plants/destroy', 'PlantController@massDestroy')->name('plants.massDestroy');
    Route::resource('plants', 'PlantController', ['except' => ['edit', 'update']]);

    // Image
    Route::delete('images/destroy', 'ImageController@massDestroy')->name('images.massDestroy');
    Route::post('images/media', 'ImageController@storeMedia')->name('images.storeMedia');
    Route::post('images/ckmedia', 'ImageController@storeCKEditorImages')->name('images.storeCKEditorImages');
    Route::resource('images', 'ImageController');

    // Phenologiaca Observation
    Route::delete('phenologiaca-observations/destroy', 'PhenologiacaObservationController@massDestroy')->name('phenologiaca-observations.massDestroy');
    Route::resource('phenologiaca-observations', 'PhenologiacaObservationController');

    // Event Genre
    Route::delete('event-genres/destroy', 'EventGenreController@massDestroy')->name('event-genres.massDestroy');
    Route::resource('event-genres', 'EventGenreController');

    // User Type
    Route::delete('user-types/destroy', 'UserTypeController@massDestroy')->name('user-types.massDestroy');
    Route::resource('user-types', 'UserTypeController');

    // Contacts
    Route::delete('contacts/destroy', 'ContactsController@massDestroy')->name('contacts.massDestroy');
    Route::resource('contacts', 'ContactsController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
Route::get('galleria', 'GalleriaController@index');
Route::get('home', 'HomePageController@index');
Route::get('ricerca', 'RicercaController@index');
//Route::get('formulario', 'FormularioController@index');
Route::view('formulario', 'formulario');
Route::post('submit', 'FormularioController@salva');
Route::view('amministrazione', 'amministrazione');
Route::post('submit', 'AmministrazioneController@saveDataPlants');
Route::view('registrazione', 'registrazione');
Route::post('submit', 'RichiestaDatiController@login');
//Route::view('home', 'home');
//Route::post('home', 'AdminControllerController@registrati');


