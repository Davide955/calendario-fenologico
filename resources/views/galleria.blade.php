<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galleria</title>
<link rel="stylesheet" type="text/css" href="{{ url('css/gallery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/style.css')}}">
<!-- CSS only -->
</head>
<body>
<header>
    <div>
        {{--                Menu del sito per accedere alle seguenti pagine--}}
        {{--                - Home--}}
        {{--                - Pagina di ricerca--}}
        {{--                - Galleria--}}
        {{--                - Amministrazione--}}
        {{--                - Registrzione--}}
        <nav>
            <ul>
                <li><a href="home">Home</a></li>
                <li><a href="#">Pagina di ricerca</a></li>
                <li><a href="galleria">Galleria</a></li>
                <li><a href="amministrazione">Amministrazione</a></li>
                <li><a href="formulario">Contatti</a></li>
                <li><a href="registrazione">Registrati</a></li>
            </ul>
        </nav>
    </div>
</header>
{{--Inizio main--}}
<div class="main">
    <h1>Calendario Fenologico</h1>
    <hr>
    <h2>Galleria piante</h2>
    <div class="row">
{{--        Con un foreach andiamo a mostrare le immagini prese da db--}}
        @foreach($images as $key => $image)
        <div class="column">
            <div class="content">
<!--                --><?php
//                $image = \App\Models\Image::select('file_image')
//                    ->get();
//                ?>

{{--                {{ $image->id ?? '' }} --}}
{{--                Se il file dell'immagine esiste la mostra con una larghezza di 200 em, e un altezza di 140em--}}
                @if($image->file_image)
                    <a href="{{ $image->file_image->getUrl() }}" target="_blank" style="display: inline-block" >
                        <img src="{{ $image->file_image->getUrl() }} " width="200em" height="140em" >
                    </a>
                @endif
{{--                <img id="myImg" src="../img/foresta.jpg" alt="Nature" style="width:100%; max-height:100%;">--}}
{{--                <div id="myModal" class="modal">--}}
{{--                    <span class="close">&times;</span>--}}
{{--                    <img class="modal-content" id="img01">--}}
{{--                    <div id="caption"></div>--}}
{{--                </div>--}}
                <h3>{{$image->file_path}}</h3>
                <p>{{$image->description}}</p>
            </div>
        </div>
        @endforeach
    </div>
    <!-- Fine Main-->
</div>
</body>
</html>













