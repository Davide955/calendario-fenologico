<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galleria</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
    <script src="https://d3js.org/d3.v7.min.js"></script>
    <!-- CSS only -->
</head>
<body>
<header>
    <div>
        {{--                Menu del sito per accedere alle seguenti pagine--}}
        {{--                - Home--}}
        {{--                - Pagina di ricerca--}}
        {{--                - Galleria--}}
        {{--                - Amministrazione--}}
        {{--                - Registrzione--}}
        <nav>
            <ul>
                <li><a href="home">Home</a></li>
                <li><a href="#">Pagina di ricerca</a></li>
                <li><a href="galleria">Galleria</a></li>
                <li><a href="amministrazione">Amministrazione</a></li>
                <li><a href="formulario">Contatti</a></li>
                <li><a href="registrati">Contatti</a></li>
            </ul>
        </nav>
    </div>
</header>
</body>
</html>

<?php


