<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
    <title>Formulario</title>
</head>
<body>

<div class="container-fluid vh-100">
    <div class="row">
        <header>
            <div>
                <nav>
                    <ul>
                        <li><a href="home">Home</a></li>
                        <li><a href="ricerca">Pagina di ricerca</a></li>
                        <li><a href="galleria">Galleria</a></li>
                        <li><a href="amministrazione">Amministrazione</a></li>
                        <li><a href="formulario">Contatti</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">

            <form action="login" method="POST">
                @csrf
                <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Inserisci la tua email">
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Inserisci la tua password">
                </div>
                <button type="submit" class="mt-5 btn btn-info w-100">Login</button>
            </form>
        </div>
    </div>
</div>

{{--<form action="submit" method="POST">--}}
{{--    @csrf--}}
{{--    <input type="text" name="name" placeholder="nome">--}}
{{--    <br>--}}
{{--    <br>--}}
{{--    <input type="text" name="lastname" placeholder="cognome">--}}
{{--    <br>--}}
{{--    <br>--}}
{{--    <textarea type="text" name="req" rows="4" cols="50"></textarea>--}}
{{--    <br>--}}
{{--    <button type="submit">Invia</button>--}}
{{--</form>--}}
</body>
</html>

<?php



