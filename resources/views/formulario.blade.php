<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css')}}">
    <title>Formulario</title>
</head>
<body>
<div class="container-fluid vh-100">
    <div class="row">
        <header>
            <div>
                <nav>
                    <ul>
                        <li><a href="home">Home</a></li>
                        <li><a href="ricerca">Pagina di ricerca</a></li>
                        <li><a href="galleria">Galleria</a></li>
                        <li><a href="amministrazione">Amministrazione</a></li>
                        <li><a href="formulario">Contatti</a></li>
                        <li><a href="registrazione">Registrati</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
{{--    Creazione formulario della pagina di contatti con i seguenti campi--}}
{{--    - nome--}}
{{--    - cognome--}}
{{--    - richiesta (ad esempio richiesta di inserimento della pianta castagn nel sito--}}
{{--    La pagina è stata sviluppata con bootstrap per il centramento del formulario, l'allargamento dei campi di input --}}
{{--    e la creazione del bottone invia--}}
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <h2><strong>Inserisci la tua richiesta</strong></h2>
            <form action="submit" method="POST">
{{--                Questo token aiuta a verificare che la richiesta e l'approvazione per l'applicazione vengano--}}
{{--                fornite solo all'utente autenticato.--}}
                @csrf
                <div class="form-group">
                    <label for="Nome">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Inserisci il tuo nome">
                </div>
                <div class="form-group">
                    <label for="Cognome">Cognome</label>
                    <input type="text" class="form-control" name="lastname" placeholder="Inserisci il tuo cognome">
                </div>
                <div class="form-group">
                    <label for="Richiesta">Richiesta</label>
                    <textarea type="text" class="form-control" name="req" placeholder="Scrivi..." rows="4"
                              cols="50"></textarea>
                </div>
                <button type="submit" class="mt-5 btn btn-info w-100">Invia</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<?php
