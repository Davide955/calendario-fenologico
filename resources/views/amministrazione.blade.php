<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css')}}">
    <title>Formulario</title>
</head>
<body>
<div class="container-fluid vh-100">
    <div class="row">
        <header>
            <div>
                <nav>
                    <ul>
                        <li><a href="home">Home</a></li>
                        <li><a href="ricerca">Pagina di ricerca</a></li>
                        <li><a href="galleria">Galleria</a></li>
                        <li><a href="amministrazione">Amministrazione</a></li>
                        <li><a href="formulario">Contatti</a></li>
                        <li><a href="registrazione">Registrati</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <h1><strong>Gestione delle piante</strong></h1>
            <form action="submit" method="POST">
                @csrf
                <div class="form-group">
                    <label for="NomeScientifico">Nome Scientifico</label>
                    <input type="text" class="form-control" name="scientific_name" placeholder="Inserisci il nome scientifico">
                </div>
                <div class="form-group">
                    <label for="Specie">Specie</label>
                    <input type="text" class="form-control" name="species" placeholder="Inserisci la specie">
                </div>
                <div class="form-group">
                    <label for="EtaMinima">EtaMinima</label>
                    <input type="text" class="form-control" name="minimum_age" placeholder="Inserisci l'età minima">
                </div>
                <div class="form-group">
                    <label for="EtaMassima">EtaMassima</label>
                    <input type="text" class="form-control" name="maximum_age" placeholder="Inserisci l'età massima">
                </div>
                <div class="form-group">
                    <label for="Descrizione">Descrizione</label>
                    <textarea type="text" class="form-control" name="description" placeholder="Scrivi..." rows="4"
                              cols="50"></textarea>
                </div>
                <button type="submit" class="mt-5 btn btn-info w-100">Invia</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<?php

