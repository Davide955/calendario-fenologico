@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.plant.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.plants.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.id') }}
                        </th>
                        <td>
                            {{ $plant->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.scientific_name') }}
                        </th>
                        <td>
                            {{ $plant->scientific_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.species') }}
                        </th>
                        <td>
                            {{ $plant->species }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.description') }}
                        </th>
                        <td>
                            {{ $plant->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.minimum_age') }}
                        </th>
                        <td>
                            {{ $plant->minimum_age }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.maximum_age') }}
                        </th>
                        <td>
                            {{ $plant->maximum_age }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.plant.fields.image') }}
                        </th>
                        <td>
                            {{ $plant->image->description ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.plants.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection