@extends('layouts.admin')
@section('content')
@can('plant_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.plants.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.plant.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.plant.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Plant">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.scientific_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.species') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.description') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.minimum_age') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.maximum_age') }}
                        </th>
                        <th>
                            {{ trans('cruds.plant.fields.image') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plants as $key => $plant)
                        <tr data-entry-id="{{ $plant->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $plant->id ?? '' }}
                            </td>
                            <td>
                                {{ $plant->scientific_name ?? '' }}
                            </td>
                            <td>
                                {{ $plant->species ?? '' }}
                            </td>
                            <td>
                                {{ $plant->description ?? '' }}
                            </td>
                            <td>
                                {{ $plant->minimum_age ?? '' }}
                            </td>
                            <td>
                                {{ $plant->maximum_age ?? '' }}
                            </td>
                            <td>
                                {{ $plant->image->description ?? '' }}
                            </td>
                            <td>
                                @can('plant_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.plants.show', $plant->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan


                                @can('plant_delete')
                                    <form action="{{ route('admin.plants.destroy', $plant->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('plant_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.plants.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Plant:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection