@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.plant.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.plants.update", [$plant->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="scientific_name">{{ trans('cruds.plant.fields.scientific_name') }}</label>
                <input class="form-control {{ $errors->has('scientific_name') ? 'is-invalid' : '' }}" type="text" name="scientific_name" id="scientific_name" value="{{ old('scientific_name', $plant->scientific_name) }}">
                @if($errors->has('scientific_name'))
                    <span class="text-danger">{{ $errors->first('scientific_name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.scientific_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="species">{{ trans('cruds.plant.fields.species') }}</label>
                <input class="form-control {{ $errors->has('species') ? 'is-invalid' : '' }}" type="text" name="species" id="species" value="{{ old('species', $plant->species) }}">
                @if($errors->has('species'))
                    <span class="text-danger">{{ $errors->first('species') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.species_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.plant.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', $plant->description) }}">
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="minimum_age">{{ trans('cruds.plant.fields.minimum_age') }}</label>
                <input class="form-control {{ $errors->has('minimum_age') ? 'is-invalid' : '' }}" type="number" name="minimum_age" id="minimum_age" value="{{ old('minimum_age', $plant->minimum_age) }}" step="1">
                @if($errors->has('minimum_age'))
                    <span class="text-danger">{{ $errors->first('minimum_age') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.minimum_age_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="maximum_age">{{ trans('cruds.plant.fields.maximum_age') }}</label>
                <input class="form-control {{ $errors->has('maximum_age') ? 'is-invalid' : '' }}" type="number" name="maximum_age" id="maximum_age" value="{{ old('maximum_age', $plant->maximum_age) }}" step="1">
                @if($errors->has('maximum_age'))
                    <span class="text-danger">{{ $errors->first('maximum_age') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.maximum_age_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="image_id">{{ trans('cruds.plant.fields.image') }}</label>
                <select class="form-control select2 {{ $errors->has('image') ? 'is-invalid' : '' }}" name="image_id" id="image_id">
                    @foreach($images as $id => $entry)
                        <option value="{{ $id }}" {{ (old('image_id') ? old('image_id') : $plant->image->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('image'))
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.plant.fields.image_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection