@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.phenologiacaObservation.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.phenologiaca-observations.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="description">{{ trans('cruds.phenologiacaObservation.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}">
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.phenologiacaObservation.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="area_id">{{ trans('cruds.phenologiacaObservation.fields.area') }}</label>
                <select class="form-control select2 {{ $errors->has('area') ? 'is-invalid' : '' }}" name="area_id" id="area_id">
                    @foreach($areas as $id => $entry)
                        <option value="{{ $id }}" {{ old('area_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('area'))
                    <span class="text-danger">{{ $errors->first('area') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.phenologiacaObservation.fields.area_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="plant_id">{{ trans('cruds.phenologiacaObservation.fields.plant') }}</label>
                <select class="form-control select2 {{ $errors->has('plant') ? 'is-invalid' : '' }}" name="plant_id" id="plant_id">
                    @foreach($plants as $id => $entry)
                        <option value="{{ $id }}" {{ old('plant_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('plant'))
                    <span class="text-danger">{{ $errors->first('plant') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.phenologiacaObservation.fields.plant_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="event_genre_id">{{ trans('cruds.phenologiacaObservation.fields.event_genre') }}</label>
                <select class="form-control select2 {{ $errors->has('event_genre') ? 'is-invalid' : '' }}" name="event_genre_id" id="event_genre_id">
                    @foreach($event_genres as $id => $entry)
                        <option value="{{ $id }}" {{ old('event_genre_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('event_genre'))
                    <span class="text-danger">{{ $errors->first('event_genre') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.phenologiacaObservation.fields.event_genre_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection