@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.phenologiacaObservation.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.phenologiaca-observations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologiacaObservation.fields.id') }}
                        </th>
                        <td>
                            {{ $phenologiacaObservation->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologiacaObservation.fields.description') }}
                        </th>
                        <td>
                            {{ $phenologiacaObservation->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologiacaObservation.fields.area') }}
                        </th>
                        <td>
                            {{ $phenologiacaObservation->area->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologiacaObservation.fields.plant') }}
                        </th>
                        <td>
                            {{ $phenologiacaObservation->plant->scientific_name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.phenologiacaObservation.fields.event_genre') }}
                        </th>
                        <td>
                            {{ $phenologiacaObservation->event_genre->event_name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.phenologiaca-observations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection