@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.eventGenre.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.event-genres.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="event_name">{{ trans('cruds.eventGenre.fields.event_name') }}</label>
                <input class="form-control {{ $errors->has('event_name') ? 'is-invalid' : '' }}" type="text" name="event_name" id="event_name" value="{{ old('event_name', '') }}">
                @if($errors->has('event_name'))
                    <span class="text-danger">{{ $errors->first('event_name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.eventGenre.fields.event_name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection