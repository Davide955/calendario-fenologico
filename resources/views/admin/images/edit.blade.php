@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.image.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.images.update", [$image->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="file_image">{{ trans('cruds.image.fields.file_image') }}</label>
                <div class="needsclick dropzone {{ $errors->has('file_image') ? 'is-invalid' : '' }}" id="file_image-dropzone">
                </div>
                @if($errors->has('file_image'))
                    <span class="text-danger">{{ $errors->first('file_image') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.image.fields.file_image_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.image.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', $image->description) }}">
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.image.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="file_path">{{ trans('cruds.image.fields.file_path') }}</label>
                <input class="form-control {{ $errors->has('file_path') ? 'is-invalid' : '' }}" type="text" name="file_path" id="file_path" value="{{ old('file_path', $image->file_path) }}">
                @if($errors->has('file_path'))
                    <span class="text-danger">{{ $errors->first('file_path') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.image.fields.file_path_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.fileImageDropzone = {
    url: '{{ route('admin.images.storeMedia') }}',
    maxFilesize: 10, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="file_image"]').remove()
      $('form').append('<input type="hidden" name="file_image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="file_image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($image) && $image->file_image)
      var file = {!! json_encode($image->file_image) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, file.preview)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="file_image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection