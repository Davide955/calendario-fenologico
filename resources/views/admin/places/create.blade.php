@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.place.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.places.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="place">{{ trans('cruds.place.fields.place') }}</label>
                <input class="form-control {{ $errors->has('place') ? 'is-invalid' : '' }}" type="text" name="place" id="place" value="{{ old('place', '') }}">
                @if($errors->has('place'))
                    <span class="text-danger">{{ $errors->first('place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.place_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.place.fields.region') }}</label>
                <select class="form-control {{ $errors->has('region') ? 'is-invalid' : '' }}" name="region" id="region">
                    <option value disabled {{ old('region', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Place::REGION_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('region', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('region'))
                    <span class="text-danger">{{ $errors->first('region') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.region_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="cap">{{ trans('cruds.place.fields.cap') }}</label>
                <input class="form-control {{ $errors->has('cap') ? 'is-invalid' : '' }}" type="number" name="cap" id="cap" value="{{ old('cap', '') }}" step="1">
                @if($errors->has('cap'))
                    <span class="text-danger">{{ $errors->first('cap') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.cap_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="person_number">{{ trans('cruds.place.fields.person_number') }}</label>
                <input class="form-control {{ $errors->has('person_number') ? 'is-invalid' : '' }}" type="number" name="person_number" id="person_number" value="{{ old('person_number', '') }}" step="1">
                @if($errors->has('person_number'))
                    <span class="text-danger">{{ $errors->first('person_number') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.person_number_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection