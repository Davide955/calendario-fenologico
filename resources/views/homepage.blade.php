<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Link css e bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css')}}">

    </head>
    <body>
    <div class="container-fluid" id="table-container">
        <header>
            <div>
{{--                Menu del sito per accedere alle seguenti pagine--}}
{{--                - Home--}}
{{--                - Pagina di ricerca--}}
{{--                - Galleria--}}
{{--                - Amministrazione--}}
{{--                - Registrzione--}}
                <nav>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="ricerca">Pagina di ricerca</a></li>
                        <li><a href="galleria">Galleria</a></li>
                        <li><a href="amministrazione">Amministrazione</a></li>
                        <li><a href="formulario">Contatti</a></li>
                        <li><a href="registrazione">Registrati</a></li>
                    </ul>
                </nav>
            </div>
        </header>
{{--        Si vuole creare una tabella che mostra i dati delle piante specificandone--}}
{{--        il nome scientifico, la specie, la descrizione e l'eta minima--}}
        <table>
            <caption>Benvenuti nel nostro sito</caption>
            <thead>
                <tr>
                    <td>Nome scientifico</td>
                    <td>Specie</td>
                    <td>Descrizione</td>
                    <td>Eta minima</td>
                </tr>
            </thead>
            <tbody>
                {{--dd($plants)--}}
                {{--    @foreach($plants as $p)--}}
                {{--        <h1>{{$p->scientific_name}}</h1>--}}
                {{--    @endforeach--}}

                <?php

                //Eloquent ORM
                $result = \App\Models\Plant::select('scientific_name', 'species', 'description', 'minimum_age')
                    ->get();

                //dd($result);
                if($result->count() > 0){
//                    Con il ciclo foreach se ci sono risultati ci vengono mostrati i dati presi dal db
                    foreach($result as $row)
                    {
                        echo "<tr>";
                        echo "<td>". $row->scientific_name."</td>";
                        echo "<td>". $row->species."</td>";
                        echo "<td>". $row->description."</td>";
                        echo "<td>". $row->minimum_age."</td>";
                        echo "</tr>";

    //                        ."</td><td>". $row["species"]."</td><td>".

                        //$row["description"] ."</td><td>". $row["minimum_age"]."</td></tr>";
                    }
    //                foreach($result as $row)
    //                {
    //                    echo "<tr><td>". $row->species;
    //                    //."</td><td>". $row["species"]."</td><td>".
    //                    //$row["description"] ."</td><td>". $row["minimum_age"]."</td></tr>";
    //                }

    //                echo "</table>";
                }
                else {
                    echo "0 risultati";
                }

                ?>
                {{--    @foreach($plants as $plant)--}}
                {{--        <h4>{{$plant->scientific_name}}</h4>--}}
                {{--    @endforeach--}}
                </tbody>
            </table>
            <form action="#" method="POST">
                <div class="input-box">
                    <input type="submit" value="Accedi">
                </div>
            </form>
        </div>
    </body>
</html>


