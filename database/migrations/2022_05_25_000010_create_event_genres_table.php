<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventGenresTable extends Migration
{
    public function up()
    {
        Schema::create('event_genres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
