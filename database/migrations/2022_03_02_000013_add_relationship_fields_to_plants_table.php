<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPlantsTable extends Migration
{
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->unsignedBigInteger('image_id')->nullable();
            $table->foreign('image_id', 'image_fk_6109171')->references('id')->on('images');
        });
    }
}
