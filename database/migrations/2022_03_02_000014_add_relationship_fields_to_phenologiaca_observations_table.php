<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPhenologiacaObservationsTable extends Migration
{
    public function up()
    {
        Schema::table('phenologiaca_observations', function (Blueprint $table) {
            $table->unsignedBigInteger('area_id')->nullable();
            $table->foreign('area_id', 'area_fk_6119743')->references('id')->on('areas');
            $table->unsignedBigInteger('plant_id')->nullable();
            $table->foreign('plant_id', 'plant_fk_6119758')->references('id')->on('plants');
            $table->unsignedBigInteger('event_genre_id')->nullable();
            $table->foreign('event_genre_id', 'event_genre_fk_6127483')->references('id')->on('event_genres');
        });
    }
}
