<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('place')->nullable();
            $table->string('region')->nullable();
            $table->integer('cap')->nullable();
            $table->integer('person_number')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
