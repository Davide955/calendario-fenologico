<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAreasTable extends Migration
{
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_6058663')->references('id')->on('users');
            $table->unsignedBigInteger('place_id')->nullable();
            $table->foreign('place_id', 'place_fk_6058684')->references('id')->on('places');
        });
    }
}
