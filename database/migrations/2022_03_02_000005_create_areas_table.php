<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->float('latitude', 15, 2)->nullable();
            $table->float('longitude', 15, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
