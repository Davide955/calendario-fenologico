<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'area_create',
            ],
            [
                'id'    => 18,
                'title' => 'area_edit',
            ],
            [
                'id'    => 19,
                'title' => 'area_show',
            ],
            [
                'id'    => 20,
                'title' => 'area_delete',
            ],
            [
                'id'    => 21,
                'title' => 'area_access',
            ],
            [
                'id'    => 22,
                'title' => 'place_create',
            ],
            [
                'id'    => 23,
                'title' => 'place_edit',
            ],
            [
                'id'    => 24,
                'title' => 'place_show',
            ],
            [
                'id'    => 25,
                'title' => 'place_delete',
            ],
            [
                'id'    => 26,
                'title' => 'place_access',
            ],
            [
                'id'    => 27,
                'title' => 'plant_create',
            ],
            [
                'id'    => 28,
                'title' => 'plant_show',
            ],
            [
                'id'    => 29,
                'title' => 'plant_delete',
            ],
            [
                'id'    => 30,
                'title' => 'plant_access',
            ],
            [
                'id'    => 31,
                'title' => 'image_create',
            ],
            [
                'id'    => 32,
                'title' => 'image_edit',
            ],
            [
                'id'    => 33,
                'title' => 'image_show',
            ],
            [
                'id'    => 34,
                'title' => 'image_delete',
            ],
            [
                'id'    => 35,
                'title' => 'image_access',
            ],
            [
                'id'    => 36,
                'title' => 'phenologiaca_observation_create',
            ],
            [
                'id'    => 37,
                'title' => 'phenologiaca_observation_edit',
            ],
            [
                'id'    => 38,
                'title' => 'phenologiaca_observation_show',
            ],
            [
                'id'    => 39,
                'title' => 'phenologiaca_observation_delete',
            ],
            [
                'id'    => 40,
                'title' => 'phenologiaca_observation_access',
            ],
            [
                'id'    => 41,
                'title' => 'event_genre_create',
            ],
            [
                'id'    => 42,
                'title' => 'event_genre_edit',
            ],
            [
                'id'    => 43,
                'title' => 'event_genre_show',
            ],
            [
                'id'    => 44,
                'title' => 'event_genre_delete',
            ],
            [
                'id'    => 45,
                'title' => 'event_genre_access',
            ],
            [
                'id'    => 46,
                'title' => 'user_type_create',
            ],
            [
                'id'    => 47,
                'title' => 'user_type_edit',
            ],
            [
                'id'    => 48,
                'title' => 'user_type_show',
            ],
            [
                'id'    => 49,
                'title' => 'user_type_delete',
            ],
            [
                'id'    => 50,
                'title' => 'user_type_access',
            ],
            [
                'id'    => 51,
                'title' => 'contact_create',
            ],
            [
                'id'    => 52,
                'title' => 'contact_edit',
            ],
            [
                'id'    => 53,
                'title' => 'contact_show',
            ],
            [
                'id'    => 54,
                'title' => 'contact_delete',
            ],
            [
                'id'    => 55,
                'title' => 'contact_access',
            ],
            [
                'id'    => 56,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
